$(document).ready(function(){
  // add new task
  $('input').keypress(function(event){
    var newTask = $('input').val()
    if(event.which === 13){
      if(newTask === ''){
        alert('as')
      }else{
        $('ul').append('<li><span class="deleteButton"><i class="fa fa-trash-o" aria-hidden="true"></i></span>'+ newTask +'</li>')
        $('input').val('')
      }
    }
    event.stopPropagation();
  })
  //update task done
  $('ul').on('click','li',function(){
    $(this).toggleClass('taskDone')
  })
  // delete task
  $('ul').on('click','span',function(){
    $(this).parent().fadeOut(200,function(){
      $(this).remove()
    })
  })
  //toggle add new task
  $('#plus').click(function(){
    $('input').fadeToggle()
  })
})